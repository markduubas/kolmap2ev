//package neljapäev_17_jaan;
//
//import java.util.Date;
//
//public class T88taja {
//
//    public String name = "";
//    public double salary = 0.00;
//    public Date started = null;
//    public Date leaved = null;
//
//    public T88taja(String name, double salary, Date started, Date leaved) {
//        this.name = name;
//        this.salary = salary;
//        this.started = started;
//        this.leaved = leaved;
//
//    }
//
//    public String toString() {
//        return String.format("%s palk: %s töötab alates %td. %<tB %<tY.", name, salary, started) +
//                (leaved == null ? "" : String.format(" %s lahkus töölt %td. %<tB %<tY.", name, leaved));
//
//    }
//}