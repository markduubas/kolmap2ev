//package ee.bcs.valiit;
//// 1. Loeb massiivist või konsoolist mingi peotäie nimesid ja sünnipäev
//// (hoia neid siis massivina ntx)
//// 2. Teed kindlasks, kes nimekirjas on kõikse noorem ja ütled tema nime
//// 3. Proovid välja selgitada, kelle sünnipäev tuleb järgmisena ja ütled, kelle ja millal
//
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//
//public class Synnipäevad {
//
//
//    public static void main(String[] args) throws Exception {
//
//            SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
//
//            Calendar täna = new GregorianCalendar();
//            täna.setTime(new Date());
//
//            String[] nimed = {"Henn", "Ants", "Peeter"};
//            Date[] sünnikuupäevad = {
//                    f.parse("07-03-1955"),
//                    f.parse("06-08-1982"),
//                    f.parse("07-01-1960")
//            };
//            Calendar[] sünnipäevad = new Calendar[sünnikuupäevad.length];
//            for(int i = 0; i < sünnikuupäevad.length; i++) {
//                sünnipäevad[i] = new GregorianCalendar();
//                sünnipäevad[i].setTime(sünnikuupäevad[i]);
//                sünnipäevad[i].set(Calendar.YEAR, 2019);
//                if (sünnipäevad[i].compareTo(täna) < 1) sünnipäevad[i].set(Calendar.YEAR, 2020);
//
//            }
//
//            // peale neid operatsioone on kolm massiivi olemas
//
//            for(int i = 0; i < nimed.length; i++) {
//                System.out.printf("%s on sündinud %tB %<te, %<tY ja ta sünnipäev on %tB %<te, %<tY %n",
//                        nimed[i], sünnikuupäevad[i], sünnipäevad[i]);
//            }
//
//
//
//        }
//    }