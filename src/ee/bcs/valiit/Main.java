//package ee.bcs.valiit;
//
//import java.util.Scanner;
//
//public class Main {
//
//    public static void main(String[] args) {
//
//        // write your code here
//        Scanner scan = new Scanner(System.in);
//        String rida = "";
//
//        do {
//            System.out.println("What color is there: ");
//            rida = scan.nextLine();
//
//            switch (rida.toLowerCase()) {
//                case "red":
//                    System.out.println("Stop now!");
//                    break;
//                case "yellow":
//                    System.out.println("Start stopping!");
//                    break;
//                case "green":
//                    System.out.println("Drive!");
//                    break;
//                default:
//                    System.out.println("Wrong color!");
//            }
//        } while
//            //Remember to use "!" argument otherwise it will end the code.
//        (!rida.equalsIgnoreCase("green"));
//
//        //JAVA head kombed.
//        //Alati kui defineerid muutuja anna talle mingi algväärtus!
//
//    }
//}
