//package reede;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.Random;
//
//public class fisher_random {
//
//
//    public static void knuth(int[] array) {
//        Random r = new Random();
//        for (int i = array.length - 1; i > 0; i--) {
//            int index = r.nextInt(i);
//            // swap
//            int tmp = array[index];
//            array[index] = array[i];
//            array[i] = tmp;
//        }
//    }
//
//    public static void main(String[] args) {
//
//        int [] array = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
//
//        System.out.println("Before: " + Arrays.toString(array));
//
//        knuth(array);
//
//        System.out.println("After:  " + Arrays.toString(array));
//
//    }
//}