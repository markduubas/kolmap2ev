//package Teisipäev15jaan;
//
//import java.util.*;
//
//public class Person {
//
//    private String personalId = "00000000000";
//    private String name = "unknown";
//    //vanast listist maha
//
//
//    private static int inimesteArv = 0;
//    public static List<Person> listOfPeople = new ArrayList<>();
//
//    public static Map<String, Person> mapOfPeople = new HashMap<>();
//    public static Map<String, List<Person>> mapOfPeopleNames = new TreeMap<>();
//
//
//    public static List<Person> getName(String name) {
//        List<Person> vastus = new ArrayList<>();
//        for (var p : ListOfPeople) if (p.name.equalsIgnoreCase(name)) vastus.add(p);
//        return vastus;
//    }
//
//    public static int getInimesteArv() {
//        return inimesteArv;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//
//        if (name.length() < 1) this.name = "unknown";
//        else {
//
//            String[] osad = name.split(" ");
//            for (int i = 0; i < osad.length; i++) {
//                osad[i] = osad[i].substring(0, 1).toUpperCase() + osad[i].substring(1).toLowerCase();
//            }
//            this.name = String.join(" ", osad);
//        }
//    }
//
//    // see siin on konstruktor
//    public Person(String personalId, String name) throws Exception {
//        inimesteArv++;
//        listOfPeople.add(this); // siin lisasime inimese listi
//        // huvitav, kas seda ongi vaja tulevikus,. kui meil juba dictionarid on
//
//        setName(name);
//        this.personalId = personalId;
//
//        // iga inimene, kes konstrueeritakse, pannakse isikukoodi järgi dictionay
//        if (! mapOfPeople.keySet().contains(personalId)) {
//            mapOfPeople.put(personalId, this);
//        }
//        else {throw  new Exception("sellise isikukoodiga inimene on juba ees: " + personalId);}
//        // NB! kui sama isikukoodiga tuleb uus inimene, siis ei pane
//        // me peaks mõtlema, mis teha, et sellest ka teada anda
//
//
//        if (! mapOfPeopleNames.keySet().contains(name)) {
//            mapOfPeopleNames.put(name, new ArrayList<>());
//        }
//        mapOfPeopleNames.get(name).add(this);
//        }
//    }
//
//        public String getPersonalId () {
//            return personalId;
//        }
//        private void setPersonalId (String personalId){
//            this.personalId = personalId;
//        }
//
//        // selline on vaikimisi (default) konstruktor
//        // kui on omatehtud, siis sellist süsteem ise ei tee
//        // kui mul on mitu konstruktorit - overloadimine
//        Person() {
//            this("00000000000", "");
//        }
//
//
//    public Person(String personalId) {
//            this(personalId, "");
//        }
//
//        // siin lõppesid konstruktorid
//
//        // siit algavad meetodid
//
//        // getterid ja setterid
//
//
//        public int getBirthYear () {
//            return (((Integer.parseInt(personalId.substring(0, 1)) - 1) / 2) * 100
//                    + 1800 + Integer.parseInt(personalId.substring(1, 3)));
//        }
//
//        public Date getBirthDate () {
//            return (new GregorianCalendar(
//                    getBirthYear(),
//                    Integer.parseInt(personalId.substring(3, 5)) - 1,
//                    Integer.parseInt(personalId.substring(5, 7))
//            )).getTime();
//        }
//
//        public String getBirthMonth () {
//            // vaja leida isikukoodi järgi sünnikuu 'Märts'
//            return String.format("%tB", getBirthDate());
//
//
//        }
//
//
//        // siit algavad overridetud meetodid - mis iganes need on
//        public String toString () {
//            return String.format("%s: %s syndinud %td.%<tB %<tY.aastal.",
//                    getGender(), name, getBirthDate());
//        }
//
//        public Gender getGender () {
//            if (Integer.parseInt(personalId.substring(0, 1)) % 2 == 0)
//                return Gender.NAINE;
//            else
//                return Gender.MEES;
//        }
//
//    }
//
