//package Teisipäev15jaan;
//
//public enum Gender {
//    NAINE, MEES
//}
////    public static final int NAINE = 0;
////    public static final int MEES = 1;
//
//// Inimesed on vaja leida üles:
////
////   Asjaolud:    inimesi on palju
////                isikukoodid on unikaalsed
////                nimed võivad korduda
////
////
////   Nõuded:      Isikukoodi järgi peab olema võimalik leida see isik
////                Isikukood ei muutu
////                Kui isikukoodi ei ole, siis peaks leid olema null
////                Nimi võib muutuda
////                Nime järgi otsides tuleks leida KÕIK sellenimelised
////
////  Võimalused:   Teeme listi ja otsime sealt
////                Hea- lihtne ja me oskame
////                Halb- kulukas, kui inimesi palju
////
////                teeme mapi inimestest, kus võti on isikukood
////                teeme mapi inimestest, kus võti on nimi
////
////                hea lihtne teha, isikukoodiga isegi saaks
////                halb- võti (nimi) kipub muutuma
////                halb- nimega inimesi on palju
