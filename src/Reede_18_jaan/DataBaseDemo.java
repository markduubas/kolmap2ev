//package Reede_18_jaan;
//
//import java.sql.Connection;
//import java.sql.Statement;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.DriverManager;
//import java.util.Scanner;
//
//public class DataBaseDemo {
//
//    public static void main(String[] args) {
//
//        String hostName = "hennusql.database.windows.net"; // update me
//        String dbName = "Northwind"; // update me
//        String user = "Student"; // update me
//        String password = "Pa$$w0rd"; // update me
//        String url = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;encrypt=true;"
//                + "hostNameInCertificate=*.database.windows.net;loginTimeout=30;", hostName, dbName, user, password);
//        Connection connection = null;
//
//        try {
//            connection = DriverManager.getConnection(url);
//            String schema = connection.getSchema();
//            System.out.println("Successful connection - Schema: " + schema);
//
//            System.out.println("Query data example:");
//            System.out.println("=========================================");
//
//            Scanner scan = new Scanner(System.in);
//            System.out.print("Anna tootekood: ");
//            int productid = scan.nextInt();
//            System.out.print("Anna uus hind; ");
//            double unitprice = scan.nextDouble();
//
//            Statement update = connection.prepareStatement(updateSql);
//            ((PreparedStatement)update).setDouble(1, unitprice);
//            ((PreparedStatement)update).setDouble(2, productid);
//            var kinnitus = update.executeUpdate(updateSql);
//
//            // Create and execute a SELECT SQL statement.
//            String selectSql = "select CategoryName, ProductName, UnitPrice "
//                    + "FROM Products p "
//                    + "JOIN Categories c ON p.categoryid = c.categoryid";
//
//            try (Statement statement = connection.createStatement();
//                 ResultSet resultSet = statement.executeQuery(selectSql)) {
//
//                // Print results from select statement
//                System.out.println("Top 20 categories:");
//                while (resultSet.next()) {
//                    System.out.println(resultSet.getString(1) + "\t"
//                            + resultSet.getString(2) + "\t"
//                            + resultSet.getDouble(3));
//                }
//                connection.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//}
